import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

import routes from './components/routes';

const app = document.getElementById('app');
ReactDOM.render(<Router routes={routes} history={browserHistory} />, app);