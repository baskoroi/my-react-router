import React from 'react';

import Home from './Home';

/*import { IndexLink } from 'react-router';*/
import NavLink from './NavLink';

export default class App extends React.Component {
    render() {
        return (
            <div>
                <h1>Hello, world!</h1>
                <ul role="nav">
                    <li><NavLink to="/" onlyActiveOnIndex={true}>Home</NavLink></li>
                    <li><NavLink to="/about">About</NavLink></li>
                    <li><NavLink to="/repos">Repos</NavLink></li>
                </ul>

                {this.props.children}
            </div>
        );
    }
}