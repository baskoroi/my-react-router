import React from 'react';
import { Link } from 'react-router';

export default class NavLink extends React.Component {
	render() {
		/*console.log(this.props);*/

		// add {...this.props} to inherit this.props whose value is:
		// `Object {to: '/your-link', children: 'YourLink'}`, to the 
		// <Link> component below
		return <Link {...this.props} activeClassName="active" />;
	}
}